# BeagleY®-AI

BeagleY®-AI is a powerful 64-bit quad core single board computer with GPU, DSP, and vision/deep learning accelerator. 

| Front      |  Back |
| :-------------------------: | :-------------------------: |
| <img src="images/beagley-ai-board-front.webp" alt="drawing" width="740"/>  | <img src="images/beagley-ai-board-back.webp" alt="drawing" width="740"/>|

OSHW Certification: https://certification.oshwa.org/us002616.html

Features and purchase information at https://beagley-ai.org

## Features

| Feature               | Description                                                                                                |
|-----------------------|------------------------------------------------------------------------------------------------------------|
| Processor             | TI AM67 with Quad core 64-bit Arm® Cortex®-A53, GPU, DSP, and vision/deep learning accelerators            |
| RAM                   | 4GB LPDDR4                                                                                                 |
| Wi-Fi                 | BeagleBoard BM3301, 802.11ax Wi-Fi                                                                         |
| Bluetooth             | Bluetooth Low Energy 5.4 (BLE)                                                                             |
| USB Ports             | 4 x USB 3.0 TypeA ports supporting simultaneous 5Gbps operation, 1 x USB 2.0 TypeC supports USB 2.0 device |
| Ethernet              | Gigabit Ethernet, with PoE+ support (requires separate PoE+ HAT)                                           |
| Camera/Display        | 1 x 4-lane MIPI camera/display transceivers, 1 x 4-lane MIPI camera                                        |
| Display Output        | 1 x HDMI display, 1 x OLDI display                                                                         |
| Real-time Clock (RTC) | Supports an external button battery for power failure time retention. it is only populated on EVT samples. |
| Debug UART            | 1 x 3-pin debug UART                                                                                       |
| Power                 | 5V/5A DC power via USB-C, with Power Delivery support                                                      |
| Power Button          | On/Off included                                                                                            |
| PCIe Interface        | PCI-Express® Gen3 x 1 interface for fast peripherals (requires separate M.2 HAT or other adapter)          |
| Expansion Connector   | 40-pin header                                                                                              |
| Fan connector         | 1 x 4-pin fan connector, supports PWM speed control and speed measurement                                  |
| Storage               | microSD card slot, with support for high-speed SDR104 mode                                                 |
| Tag Connect           | 1 x JTAG, 1 x Tag Connect for PMIC NVM Programming                                                         |

## Components location

### Front

![BegleY-AI front annotated](images/beagley-ai-board-front-annotated.webp)


### Back

![BeagleY-AI back annotated](images/beagley-ai-board-back-annotated.webp)

## Block diagram

![BeagleY-AI Block Diagram](design/BeagleY-AI_Rev_A_BLOCK_DIAGRAM_240426.png)

## Mechanical Drawing

![BeagleY-AI Mechanical Drawing](design/mechanical_drawing.png)